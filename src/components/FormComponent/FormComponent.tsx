import { useState } from 'react';
import { sendPost } from '../../api/api';

const FormComponent = () => {
  const [titulo, setTitulo] = useState('');
  const [post, setPost] = useState('');

  const onTituloChange = (e: any) => {
    setTitulo(e.target.value)
  }

  const onPostChange = (e: any) => {
    setPost(e.target.value)
  }

  const onSubmit = (e: any) => {
    e.preventDefault()
    sendPost({
      titulo,
      post
    })

    setTitulo('')
    setPost('')
  }

  return (
    <form onSubmit={onSubmit}>
      <label 
        htmlFor="titulo" 
        style={{display: 'block'}}>Titulo
        <input 
          type="text" 
          onChange={onTituloChange}
          name="titulo" 
          value={titulo} 
          id="titulo"/>
      </label>
      <label 
        htmlFor="post" 
        style={{display: 'block'}}>Post
        <textarea 
          name="post" 
          onChange={onPostChange}
          value={post} 
          id="post"></textarea>
      </label>
      <button type='submit'>Submit</button>
    </form>
  )
}

export default FormComponent;