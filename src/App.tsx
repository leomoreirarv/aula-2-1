import FormComponent from "./components/FormComponent/FormComponent";

const App = () => {
  return (
    <FormComponent />
  );
}

export default App;
