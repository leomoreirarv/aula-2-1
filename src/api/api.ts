interface IPost {
  titulo: string;
  post: string;
}

export const sendPost = (post: IPost) => {
  fetch('http://localhost:3001/posts', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify( {
      titulo: post.titulo,
      post: post.post
    })
  })
  .then(() => console.log('post enviado'))
  .catch(() => console.error('Deu ruim'))
}